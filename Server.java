import java.net.*;

public class Server {
    static int PORT = 8080 ;
    public static void main(String[] args) throws Exception{
        
        DatagramSocket serverSocket = new DatagramSocket(PORT);

        while(true){
            System.out.println("Waiting for Clients on Port "+PORT);

            DatagramPacket receivePacket1 = new DatagramPacket(new byte[1024], 1024);
            serverSocket.receive(receivePacket1);

            InetAddress p1IPAddress = receivePacket1.getAddress();
            int p1Port = receivePacket1.getPort();
            System.out.println("RECEIVED: " + p1IPAddress.toString() + ":" + p1Port);

            System.out.println("Waiting for Clients on Port "+PORT);
            
            DatagramPacket receivePacket2 = new DatagramPacket(new byte[1024], 1024);
            serverSocket.receive(receivePacket2);

            InetAddress p2IPAddress = receivePacket2.getAddress();
            int p2Port = receivePacket2.getPort();
            String msgInfoOfClient2 = p2IPAddress.toString().substring(1) + ":" + p2Port + ":" + p1IPAddress.toString().substring(1) + ":"+ p1Port + ":end";
            System.out.println("RECEIVED: " + p2IPAddress.toString() + ":" + p2Port);

            
            String msgInfoOfClient1 = p1IPAddress.toString().substring(1) + ":" + p1Port + ":" + p2IPAddress.toString().substring(1) + ":"+ p2Port + ":end";

            
            serverSocket.send(new DatagramPacket(msgInfoOfClient1.getBytes(), msgInfoOfClient1.getBytes().length, p1IPAddress, p1Port));

            
            serverSocket.send(new DatagramPacket(msgInfoOfClient2.getBytes(), msgInfoOfClient2.getBytes().length, p2IPAddress, p2Port));

        }

    }
}
