import java.io.*;
import java.net.*;
import java.nio.channels.DatagramChannel;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.Scanner;

public class Client extends Thread{
    static Scanner input = new Scanner (System.in);
    String activity;
    static String myIp , peerIp;
    static int myPort , peerPort;
    static String serverName ;
    static int serverPort;
    static String username;
    public Client(String activity){
        this.activity = activity;
    }

    public void run(){
        try{

            if(activity == "listen"){
                peerListen();
            }else{
                peerSend();
            }
        }catch(Exception e){
            e.printStackTrace();
        }

    }
    public static void initClient(){
        try {
           
            DatagramSocket clientSocket = new DatagramSocket();
            clientSocket.setReuseAddress(true);
            byte[] sendData = "Hello".getBytes();
            System.out.println("sending Hello to server");
            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, InetAddress.getByName(serverName), serverPort);
            clientSocket.send(sendPacket);
            DatagramPacket receivePacket = new DatagramPacket(new byte[1024], 1024);
            clientSocket.receive(receivePacket);
            System.out.println("received data from server");
            clientSocket.close();
            String response = new String(receivePacket.getData());
            String[] splitResponse = response.split(":");
            myIp = splitResponse[0];
            myPort = Integer.parseInt(splitResponse[1]);
            peerIp = splitResponse[2];
            peerPort = Integer.parseInt(splitResponse[3]);

            System.out.println("my Info: " + myIp + ":" + myPort );
            System.out.println("peer Info: " + peerIp + ":" + peerPort );
            System.out.println("\n\n");
            
            Thread listen = new Client("listen");
            listen.start();
            Thread.sleep(2000);

            Thread send = new Client("send");
            send.start();
            
            listen.join();
            send.join();
        }catch (Exception e){

        }
    }
    private static void peerSend() {

        try {
            Socket mySoc = new Socket();
            mySoc.setReuseAddress(true);

            mySoc.connect( new InetSocketAddress(peerIp, peerPort) );
            pause();

            DataOutputStream out = new DataOutputStream( mySoc.getOutputStream() );
            DataInputStream in = new DataInputStream( mySoc.getInputStream() );


            while(true){
                try{
                System.out.println("Message : ");
                String msg = input.nextLine();
                out.writeUTF(msg);
                }catch(SocketException e){
                    System.out.println("I can not send because peer is not connected");
                    System.exit(0);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void peerListen() throws Exception{
        System.out.println("Listening on Socket: " + myPort);
        ServerSocket peerSocket = new ServerSocket();
        peerSocket.setReuseAddress(true);
        peerSocket.bind( new InetSocketAddress(myIp, myPort) );
        peerSocket.setReuseAddress(true);
        Socket peer = peerSocket.accept();

        System.out.println("Just connected with peer");

        DataInputStream in = new DataInputStream(peer.getInputStream());
        DataOutputStream out = new DataOutputStream(peer.getOutputStream());
        while(true){
            try{
            String msg = in.readUTF();
            System.out.println(username+" : "+msg);
            }catch(SocketException e){
                System.out.println("I can not receive because peer is not connected");
                System.exit(0);
            }
        }
    }

    private static String getTime(){
        DateFormat df = new SimpleDateFormat("hh:mm:ss");
        Date dateobj = new Date();
        return df.format(dateobj);
    }

    private static void pause()throws Exception{
        Random rand = new Random();
        int breath = rand.nextInt(5) + 1;
        Thread.sleep(breath*10);
    }
}
